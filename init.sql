CREATE DATABASE task2_db;

USE task2_db;

CREATE TABLE table (
	student_name varchar(255) NOT NULL,
	math varchar(1),
	biology varchar(1),
	history varchar(1),
	geography varchar(1),
	pe varchar(1)
);

INSERT INTO table (student_name,math,biology,history,geography,pe)
VALUES
("Joel Jordan","5","4","3","5","5"),
("Gloria Leach","4","2","5",5","2"),
("Gladys Wilkins","3","3","5","3","2"),
("Fatima Reese","5","4","4","4","4"),
("Bilal Joseph","5","3","3","5","4"),
("Abbey Olsen","3","2","5","5","2"),
("Malika Richard","2","2","2","2","5"),
("Lila Poole","4","5","3","2","5"),
("Darius Wright","5","5","5","5","5");

COMMIT;
