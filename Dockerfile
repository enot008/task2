FROM python:3.10
WORKDIR /task2
COPY task2.py ./
COPY answer.txt ./
RUN pip install mysql-connector-python
CMD ["python", "./task2.py"]
